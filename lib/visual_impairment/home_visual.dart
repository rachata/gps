import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_application_14/visual_impairment/login_visual.dart';
import 'package:flutter_application_14/visual_impairment/setting_visual.dart';
import 'package:flutter_application_14/visual_impairment/warning_visual.dart';
import 'package:geolocator/geolocator.dart';
import 'package:quickalert/models/quickalert_type.dart';
import 'package:quickalert/widgets/quickalert_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:intl/intl.dart';
class HomeVisual extends StatefulWidget {


  @override
  State<HomeVisual> createState() => _HomeVisualState();


}

class _HomeVisualState extends State<HomeVisual> {

   SharedPreferences ? prefs;

   String ? username = "";
  @override
  void initState() {

    getName();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromARGB(255, 157, 204, 185),
      body:  Column(
        children: [
          SizedBox(height: 20 / 2),
          Expanded(
            child: Stack(
              children: [
                Container(
                  margin: EdgeInsets.only(top: 270),
                  decoration: BoxDecoration(
                    color: Color.fromARGB(255, 223, 248, 239),
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(70),
                      topRight: Radius.circular(70),
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => RegisterVisual(
                            showLoginPage: () {},
                          )),
                    );
                  },
                  child: Align(
                    alignment: Alignment.center,
                    child: Container(
                      margin: EdgeInsets.only(top: 100),
                      padding: EdgeInsets.symmetric(horizontal: 0, vertical: 0),
                      decoration: BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.5),
                            blurRadius: 5,
                          ),
                        ],
                      ),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(20),
                        child: Image.asset(
                          "assets/images/aa.jpg",
                          width: 370,
                          height: 230,
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                ),
                Positioned(
                  top: 540,
                  right: 50,
                  child: GestureDetector(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => RegisterVisual(
                              showLoginPage: () {},
                            )),
                      );
                    },
                    child: Image.asset(
                      "assets/images/aaaaa.png",
                      fit: BoxFit.contain,
                      height: 90,
                      width: 90,
                    ),
                  ),
                ),
                Positioned(
                  top: 130,
                  right: 80,
                  left: 80,
                  child: GestureDetector(
                    child: Image.asset(
                      "assets/images/aa.png",
                      fit: BoxFit.contain,
                      height: 180,
                      width: 180,
                    ),
                  ),
                ),
                Positioned(
                  top: 255,
                  left: 120,
                  right: 0,
                  child: Image.asset(
                    "assets/images/eye.png",
                    fit: BoxFit.contain,
                    height: 45,
                    width: 45,
                  ),
                ),
                Positioned(
                  top: 310,
                  right: 80,
                  left: 80,
                  child: Align(
                    alignment: Alignment.center,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "${username}",
                          style: TextStyle(
                            fontSize: 22,
                            fontWeight: FontWeight.bold,
                            color: Color.fromARGB(255, 9, 116, 34),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Positioned(
                  top: 640,
                  right: 70,
                  left: 70,
                  child: Align(
                    alignment: Alignment.center,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "บริการเหตุฉุกเฉิน",
                          style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                            color: Color.fromARGB(255, 13, 177, 65),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Positioned(
                  top: 680,
                  right: 20,
                  left: 20,
                  child: Align(
                    alignment: Alignment.center,
                    child: GestureDetector(
                      onTap: () {

                        _determinePosition();


                      },
                      child: Container(
                        width: 400,
                        height: 55,
                        padding:
                        EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                        decoration: BoxDecoration(
                          color: Color.fromARGB(255, 20, 135, 20),
                          borderRadius: BorderRadius.circular(30),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Image.asset(
                              "assets/images/soss.png",
                              fit: BoxFit.contain,
                              height: 30,
                              width: 30,
                            ),
                            SizedBox(width: 15),
                            Text(
                              "ข้อความฉุกเฉิน",
                              style: TextStyle(
                                fontSize: 22,
                                fontWeight: FontWeight.bold,
                                color: Color.fromARGB(255, 255, 255, 255),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                Positioned(
                  top: 40,
                  right: 25,
                  child: GestureDetector(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => SettingVisual()),
                      );
                    },
                    child: Image.asset(
                      "assets/images/aaaa.png",
                      fit: BoxFit.contain,
                      height: 55,
                      width: 55,
                    ),
                  ),
                ),
                Positioned(
                  top: 40,
                  right: 85,
                  child: GestureDetector(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => WarningVisual()),
                      );
                    },
                    child: Image.asset(
                      "assets/images/bell-l.png",
                      fit: BoxFit.contain,
                      height: 55,
                      width: 55,
                    ),
                  ),
                ),
                Positioned(
                  top: 30,
                  left: 30,
                  right: 0,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset(
                        "assets/images/bbb.png",
                        fit: BoxFit.contain,
                        height: 70,
                        width: 70,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Future<void> _determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;


    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {

      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {

        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {

      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }

    QuickAlert.show(
      barrierDismissible: false,
      context: context,
      type: QuickAlertType.loading,
      title: 'กำลังดึงตำแหน่ง',
      text: 'รอสักครู่...',
    );


   var position =  await Geolocator.getCurrentPosition();



   String? email = prefs!.getString("_email");



    DateTime now = DateTime.now();
    String formattedDate = DateFormat('dd-MM-yyyy HH:mm:ss').format(now);



    await FirebaseFirestore.instance
        .collection("emergency")
        .doc(email).set({
      "log" : FieldValue.arrayUnion([{'username': username,
        "dt" : formattedDate,
        'latitude': position.latitude,
        'longitude': position.longitude}])

    },SetOptions(merge: true));





    Navigator.pop(context);

    QuickAlert.show(
      barrierDismissible: false,
      context: context,
      type: QuickAlertType.success,
      title: 'ส่งตำแหน่งเรียบร้อย',
    );








  }

  Future<void> getName() async {
    prefs =  await SharedPreferences.getInstance();


    setState(() {
      username = prefs!.getString("_username");

    });
  }

}



