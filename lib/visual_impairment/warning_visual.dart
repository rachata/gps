import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../caregiver/googlemap_page.dart';
import 'home_visual.dart';

class WarningVisual extends StatefulWidget {
  @override
  State<WarningVisual> createState() => _WarningVisualState();
}

class _WarningVisualState extends State<WarningVisual> {
  List<Map<String, dynamic>> log = [];

  @override
  void initState() {
    getData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromARGB(255, 157, 204, 185),
      body: Column(
        children: [
          const SizedBox(height: 20 / 2),
          Expanded(
            child: Stack(
              children: [
                Container(
                    margin: const EdgeInsets.only(top: 110),
                    padding: EdgeInsets.only(top: 50),
                    decoration: const BoxDecoration(
                      color: Color.fromARGB(255, 223, 248, 239),
                      borderRadius: BorderRadius.only(
                        topRight: Radius.circular(70),
                      ),
                    ),
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height,
                    child: ListView.builder(
                      itemCount: log.length,
                      itemBuilder: (BuildContext context, int index) {
                        return ListTile(
                          trailing: GestureDetector(onTap: (){

                            Navigator.pushReplacement(
                                context, MaterialPageRoute(builder: (_) =>  GooglemapPage(log: [log[index]])));

                          }, child: Icon(Icons.add_alert_rounded , color: Colors.amber,),),
                          title: Text("${log[index]['dt']}"),
                          subtitle: Text(
                              "latitude ${log[index]['latitude']} longitude ${log[index]['longitude']}"),
                          onTap: () {},
                        );
                      },
                    )),
                Positioned(
                  top: 90,
                  right: 80,
                  child: Align(
                    alignment: Alignment.center,
                    child: GestureDetector(
                      child: Container(
                        width: 220,
                        height: 50,
                        padding: const EdgeInsets.symmetric(
                            horizontal: 20, vertical: 10),
                        decoration: BoxDecoration(
                          color: const Color.fromARGB(255, 255, 255, 255),
                          borderRadius: BorderRadius.circular(30),
                          boxShadow: [
                            BoxShadow(
                              color: const Color.fromARGB(255, 163, 163, 162)
                                  .withOpacity(0.5),
                              offset: const Offset(2, 2),
                              blurRadius: 2,
                              spreadRadius: 1,
                            ),
                          ],
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Image.asset(
                              "assets/images/bell-l.png",
                              fit: BoxFit.contain,
                              height: 30,
                              width: 30,
                            ),
                            const SizedBox(width: 5),
                            const Text(
                              "การแจ้งเตือน",
                              style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                                color: Color.fromARGB(255, 201, 148, 1),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                Positioned(
                  top: 40,
                  left: 20,
                  right: 0,
                  child: GestureDetector(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => HomeVisual()),
                      );
                    },
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Row(
                          children: [
                            Image.asset(
                              "assets/images/angle-left.png",
                              fit: BoxFit.contain,
                              height: 30,
                              width: 30,
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Future<void> getData() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    String? email = prefs.getString("_email");

    FirebaseFirestore.instance
        .collection("emergency")
        .doc(email)
        .get()
        .then((value) {
      setState(() {
        for (int i = 0; i < value.data()!['log'].length; i++) {
          log.add(value.data()!['log'][i]);
        }

        log = log.reversed.toList();
      });
    });

  }
}
