import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_application_14/caregiver/googlemap_page.dart';

import 'home_caregiver.dart';

class WarningCaregiver extends StatefulWidget {
  @override
  State<WarningCaregiver> createState() => _WarningCaregiverState();
}

class _WarningCaregiverState extends State<WarningCaregiver> {
  List<Map<String, dynamic>> log = [];

  @override
  void initState() {
    getData();
    super.initState();
  }

  Future<void> getData() async {

    setState(() {
      FirebaseFirestore.instance.collection("emergency").get().then((value) {
        for (int i = 0; i < value.docs.length; i++) {
          for (int j = 0; j < value.docs[i].data()['log'].length; j++) {

            setState(() {
              log.add(value.docs[i].data()['log'][j]);
            });

          }
        }


      });
    });

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromARGB(255, 157, 204, 185),
      body: Column(
        children: [
          const SizedBox(height: 20 / 2),
          Expanded(
            child: Stack(
              children: [
                Container(
                  margin: const EdgeInsets.only(top: 110),
                  decoration: const BoxDecoration(
                    color: Color.fromARGB(255, 223, 248, 239),
                    borderRadius: BorderRadius.only(
                      topRight: Radius.circular(70),
                    ),
                  ),
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height,
                    child: ListView.builder(
                      itemCount: log.length,
                      itemBuilder: (BuildContext context, int index) {
                        return ListTile(
                          trailing: GestureDetector(onTap: (){

                            Navigator.pushReplacement(
                                context, MaterialPageRoute(builder: (_) =>  GooglemapPage(log: [log[index]])));
                            
                          }, child: Icon(Icons.add_alert_rounded , color: Colors.amber,),),
                          title: Text("${log[index]['username']}"),
                          subtitle: Text(
                              "${log[index]['dt']} \nlatitude ${log[index]['latitude']} longitude ${log[index]['longitude']}"),
                          onTap: () {},
                        );
                      },
                    )),
                Positioned(
                  top: 90,
                  right: 80,
                  child: Align(
                    alignment: Alignment.center,
                    child: GestureDetector(
                      child: Container(
                        width: 220,
                        height: 50,
                        padding: const EdgeInsets.symmetric(
                            horizontal: 20, vertical: 10),
                        decoration: BoxDecoration(
                          color: const Color.fromARGB(255, 255, 255, 255),
                          borderRadius: BorderRadius.circular(30),
                          boxShadow: [
                            BoxShadow(
                              color: const Color.fromARGB(255, 163, 163, 162)
                                  .withOpacity(0.5),
                              offset: const Offset(2, 2),
                              blurRadius: 2,
                              spreadRadius: 1,
                            ),
                          ],
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Image.asset(
                              "assets/images/bell-l.png",
                              fit: BoxFit.contain,
                              height: 30,
                              width: 30,
                            ),
                            const SizedBox(width: 5),
                            const Text(
                              "การแจ้งเตือน",
                              style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                                color: Color.fromARGB(255, 201, 148, 1),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                Positioned(
                  top: 40,
                  left: 20,
                  right: 0,
                  child: GestureDetector(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => HomeCaregiver()),
                      );
                    },
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Row(
                          children: [
                            Image.asset(
                              "assets/images/angle-left.png",
                              fit: BoxFit.contain,
                              height: 30,
                              width: 30,
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
