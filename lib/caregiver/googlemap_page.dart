import 'dart:async';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class GooglemapPage extends StatefulWidget {
  final List<Map<String, dynamic>> log;

  const GooglemapPage({super.key, required this.log});
  @override
  _GooglemapPageState createState() => _GooglemapPageState();
}

class _GooglemapPageState extends State<GooglemapPage> {

  Set<Marker> _marker = {};
  final Completer<GoogleMapController> _controller =
      Completer<GoogleMapController>();

  static const CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(10.534928, 100.624263),
    zoom: 6.4746,
  );

  @override
  void initState() {
    addMarker();
    super.initState();
  }

  void addMarker() {


    for(int i = 0; i< widget.log.length ;i++){


    _marker.add( Marker(
      markerId:  MarkerId("${i}"),
      position:  LatLng(widget.log[i]['latitude'], widget.log[i]['longitude']),
    ));


    }

  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GoogleMap(
        mapType: MapType.normal,
        initialCameraPosition: _kGooglePlex,
        markers: _marker,
        onMapCreated: (GoogleMapController controller) {
          _controller.complete(controller);
        },
      ),
    );
  }




}
